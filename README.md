### Hui是一款移动UI框架，兼容Android 4.0+，IOS7+，Chrome，FF，Opera，IE10+。


### Hui 开发者信息

*****

* 框架名称：`Hui`
* 框架作者：`新生帝`
* 作者Q Q：`8020292`
* QQ交流群：`18863883`，加群请注明：`Hui`
* 开发日期：`2016年03月13日`
* 版权所有：`中山赢友网络科技有限公司`
* 企业官网：[http://www.winu.net](http://www.winu.net)
* 开源协议：`GPL v2 License`
* 系统描述：`一切从简，只为了更懒！`

*****

### Hui更新记录：

```
 ### 2016年03月19日 V1.0RC2

* [新增] 字体图标
* [新增] 大量常用样式
* [新增] 底部导航栏带角标
* [新增] 带搜索的头部
* [优化] 按钮样式
* [优化] 大量样式命名、注释
 
 **************************

 ```

### Hui公测版本：

* IOS：[IOS下载](http://downloadpkg.apicloud.com/app/download?path=http://7xrvzc.com1.z0.glb.clouddn.com/375320d7facdd17f4e86ea6033375f9.ipa)
* Android：[Android下载](http://downloadpkg.apicloud.com/app/download?path=http://7xrvzc.com1.z0.glb.clouddn.com/9627a86899274ecfc01af77e2c3da427_d)

*****

### Hui 友情捐赠

*****

如果你觉得 Hui 对你有价值，并且愿意让她继续成长下去，你可以资助这个项目的开发，为作者加油打气。

![友情捐赠Hui](http://git.oschina.net/uploads/images/2016/0207/160936_8f2d5f2e_526496.png "友情捐赠Hui")

*****

如果你喜欢Hui，可以点击右上角的`star`，想实时关注进度，可以点击右上角的`watch`。

最后，感谢每一个提建议和使用或者捐赠的朋友！因为你们，我们才能坚持！也是因为你们，未来才会更美好！

### Hui部分效果图：

![输入图片说明](http://git.oschina.net/uploads/images/2016/0319/153521_ae914ebb_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0319/153531_799ff6ad_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0319/153539_ab7546de_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0318/174955_c98adeb9_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0318/175005_0baaeac3_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0318/175136_64807e4c_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0318/175144_3d791069_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0318/175151_7f5ba293_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0318/175201_45a51e25_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0318/175210_b57274a5_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0318/175218_6deedf79_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0318/175014_cb0bd186_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0318/175022_9ad06f2b_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0318/175031_b003adbb_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0318/175040_73a87050_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0318/175048_4de53f0c_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0318/175054_ae027191_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0318/175101_7bdff864_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0318/175107_80d4b781_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0318/175114_24a9b90e_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0318/175122_ea4027dc_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0318/175128_79c6f069_526496.png "在这里输入图片标题")